#! /usr/bin/expect -f
#Help (){
#    echo "command run:"
#    echo "sh runWpcli.sh <Run on web> <path To key> <user> <server_address> <port> <web location in server> <Name_of_plugin> <--activate>"
#}

NAME=$1;
USER=$2;
ADDRESS=$3;
PORT=$4;
LOCATIONPATH=$5;
PLUGIN=$6;
ACTIVATE=$7;

echo "Run on $NAME \r";
echo "Run on $PLUGIN \r";
# expect "Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent permitted by applicable law." 
# expect "~>"
ssh -T $USER@$ADDRESS -p $PORT "cd $LOCATIONPATH && wp plugin install $PLUGIN $ACTIVATE;"

# echo "system loged";
# echo "Location: "${LOCATION_PATH}";
# cd ${LOCATION_PATH} && wp plugin install ${PLUGIN} --activate;
# expect "Success: Installed 1 of 1 plugins.";
# EOF